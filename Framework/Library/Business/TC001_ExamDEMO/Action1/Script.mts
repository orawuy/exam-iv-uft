﻿'--------------------- Data ---------------------------------------'
iRowCount = Datatable.getSheet("TC001_ExamDEMO [TC001_ExamDEMO]").getRowCount
Username = Trim((DataTable("Username","TC001_ExamDEMO [TC001_ExamDEMO]")))
Password = Trim((DataTable("Password","TC001_ExamDEMO [TC001_ExamDEMO]")))
OrderNote = Trim((DataTable("OrderNote","TC001_ExamDEMO [TC001_ExamDEMO]")))
AddProduct = Trim((DataTable("AddProduct","TC001_ExamDEMO [TC001_ExamDEMO]")))
DeleteProduct = Trim((DataTable("DeleteProduct","TC001_ExamDEMO [TC001_ExamDEMO]")))

'--------------------- Data ---------------------------------------'

Call FW_TransactionStart("OpenWeb")
Call FW_OpenWebBrowser("https://demostore.x-cart.com/","CHROME")
Call FW_TransactionEnd("OpenWeb")
 @@ script infofile_;_ZIP::ssf1.xml_;_
Call FW_TransactionStart("Click Sign in / Sign up")
Call FW_WebButton("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","Sign in / Sign up")
Call FW_TransactionEnd("Click Sign in / Sign up")

Call FW_TransactionStart("Input Username")
Call FW_WebEdit("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","Username",Username)
Call FW_TransactionEnd("Input Username")
 @@ script infofile_;_ZIP::ssf2.xml_;_
Call FW_TransactionStart("Input Password")
Call FW_WebEdit("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","Password",Password)
Call FW_TransactionEnd("Input Password")
 @@ script infofile_;_ZIP::ssf3.xml_;_
Call FW_TransactionStart("Submit Login")
Call FW_WebButton("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","Submit Login")
Call FW_TransactionEnd("Submit Login")

Call FW_TransactionStart("Select Bestsellers")
Call FW_Link("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","Bestsellers")
Call FW_TransactionEnd("Select Bestsellers")
 @@ script infofile_;_ZIP::ssf4.xml_;_
Call FW_TransactionStart("Select Accessories")
Call FW_Link("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","Accessories")
Call FW_TransactionEnd("Select Accessories") @@ script infofile_;_ZIP::ssf5.xml_;_
 @@ script infofile_;_ZIP::ssf6.xml_;_
Call FW_TransactionStart("Select Leather Mini Tote Bag")
Call FW_Link("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","Leather Mini Tote Bag")
Call FW_TransactionEnd("Select Leather Mini Tote Bag")
 @@ script infofile_;_ZIP::ssf7.xml_;_
wait 1
Call FW_TransactionStart("AddProduct") @@ script infofile_;_ZIP::ssf24.xml_;_
Call FW_WebNumber("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","amount",AddProduct)
Call FW_TransactionEnd("AddProduct") 

Call FW_TransactionStart("Add to cart") @@ script infofile_;_ZIP::ssf8.xml_;_
Call FW_WebButton("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","Add to cart")
Call FW_TransactionEnd("Add to cart") 
 @@ script infofile_;_ZIP::ssf9.xml_;_
wait 2
Call FW_TransactionStart("Click View cart") @@ script infofile_;_ZIP::ssf8.xml_;_
Call FW_Link("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","View cart")
Call FW_TransactionEnd("Click View cart") @@ script infofile_;_ZIP::ssf10.xml_;_

Call FW_TransactionStart("DeleteProduct") @@ script infofile_;_ZIP::ssf8.xml_;_
Call FW_WebNumber("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","amount",DeleteProduct)
Call FW_TransactionEnd("DeleteProduct") @@ script infofile_;_ZIP::ssf11.xml_;_

'Call FW_TransactionStart("Click cart") @@ script infofile_;_ZIP::ssf8.xml_;_
'Call FW_WebElement("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","cart")
'Call FW_TransactionEnd("Click cart") @@ script infofile_;_ZIP::ssf12.xml_;_
 
Call FW_TransactionStart("Click Go to checkout") @@ script infofile_;_ZIP::ssf8.xml_;_
Call FW_WebButton("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","Go to checkout")
Call FW_TransactionEnd("Click Go to checkout")  
wait 1 @@ script infofile_;_ZIP::ssf13.xml_;_
 
Call FW_TransactionStart("Click Local Pickup $0.00") @@ script infofile_;_ZIP::ssf8.xml_;_
Call FW_WebRadioGroup("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","methodId","4")
Call FW_TransactionEnd("Click Local Pickup $0.00") @@ script infofile_;_ZIP::ssf14.xml_;_
wait 1
Call FW_TransactionStart("Input OrderNote") @@ script infofile_;_ZIP::ssf8.xml_;_
Call FW_WebEdit("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","notes",OrderNote)
Call FW_TransactionEnd("Input OrderNote")   
wait 1 @@ script infofile_;_ZIP::ssf15.xml_;_
Call FW_TransactionStart("Click Proceed to payment") @@ script infofile_;_ZIP::ssf8.xml_;_
Call FW_WebButton("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","Proceed to payment")
Call FW_TransactionEnd("Click Proceed to payment") @@ script infofile_;_ZIP::ssf17.xml_;_
wait 1
Call FW_TransactionStart("Click Phone Ordering") @@ script infofile_;_ZIP::ssf8.xml_;_
Call FW_WebRadioGroup("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","methodId","4")
Call FW_TransactionEnd("Click Phone Ordering") 
wait 1 @@ script infofile_;_ZIP::ssf18.xml_;_
Call FW_TransactionStart("Click Place order") @@ script infofile_;_ZIP::ssf8.xml_;_
Call FW_WebButton("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","Place order")
Call FW_TransactionEnd("Click Place order") @@ script infofile_;_ZIP::ssf19.xml_;_
wait 1
Call FW_TransactionStart("Click page-title") @@ script infofile_;_ZIP::ssf8.xml_;_
Call FW_CheckWebElement("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","page-title")
Call FW_TransactionEnd("Click page-title")  
 @@ script infofile_;_ZIP::ssf20.xml_;_
wait 1
Call FW_TransactionStart("Click My account") @@ script infofile_;_ZIP::ssf8.xml_;_
Call FW_WebElement("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","My account")
Call FW_TransactionEnd("Click My account")   
 @@ script infofile_;_ZIP::ssf21.xml_;_

Call FW_TransactionStart("Click Log out")
Call FW_Link("TC001_ExamDEMO [TC001_ExamDEMO]","X-Cart Demo store company","X-Cart Demo store company","Log out")
Call FW_TransactionEnd("Click Log out") 
 @@ script infofile_;_ZIP::ssf22.xml_;_
 

 @@ script infofile_;_ZIP::ssf29.xml_;_
 @@ script infofile_;_ZIP::ssf32.xml_;_
