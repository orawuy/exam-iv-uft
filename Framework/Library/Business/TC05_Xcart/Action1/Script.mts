﻿'--------------------- Data ---------------------------------------'
iRowCount = Datatable.getSheet("TC04_Xcart [TC04_Xcart]").getRowCount
Username = Trim((DataTable("Username","TC04_Xcart [TC04_Xcart]")))
Password = Trim((DataTable("Password","TC04_Xcart [TC04_Xcart]")))
OrderNote = Trim((DataTable("OrderNote","TC04_Xcart [TC04_Xcart]")))

'--------------------- Data ---------------------------------------'

FW_TransactionStart "OpenWeb"
FW_OpenWebBrowser "https://demostore.x-cart.com/","CHROME" @@ script infofile_;_ZIP::ssf2.xml_;_
FW_TransactionEnd "OpenWeb"

wait 3
FW_TransactionStart "Sign in"
FW_WebElement "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Sign in / sign up"
FW_TransactionEnd "Sign in"

wait 2
FW_TransactionStart "Input Username"
FW_WebEdit "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","username",Username
FW_TransactionEnd "Sign in"

FW_TransactionStart "Input Password"
FW_WebEdit "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","password",Password
FW_TransactionEnd "Input Password"

FW_TransactionStart "Submit Login"
FW_WebButton "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Sign in" @@ hightlight id_;_Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebButton("{'url params':{'target':'login")_;_script infofile_;_ZIP::ssf3.xml_;_
FW_TransactionEnd "Submit Login"

wait 3
FW_TransactionStart "Click Hot deals"
FW_WebElement "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Hot deals" @@ hightlight id_;_Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebButton("{'url params':{'target':'login")_;_script infofile_;_ZIP::ssf6.xml_;_
FW_TransactionEnd "Click Hot deals"

FW_TransactionStart "Select Bestsellers"
FW_Link "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Bestsellers"
FW_TransactionEnd "Select Bestsellers" @@ script infofile_;_ZIP::ssf20.xml_;_

wait 3
FW_TransactionStart "Click Beauty & Health"
FW_Link "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Beauty & Health" @@ script infofile_;_ZIP::ssf21.xml_;_
FW_TransactionEnd "Click Beauty & Health"

wait 3
FW_TransactionStart "Select Cosmetics"
FW_Link "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Cosmetics" @@ script infofile_;_ZIP::ssf22.xml_;_
FW_TransactionEnd "Select Cosmetics"

FW_TransactionStart "Select Maui Moisture"
FW_Image "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Maui Moisture"
FW_TransactionEnd "Select Maui Moisture" @@ script infofile_;_ZIP::ssf23.xml_;_

FW_TransactionStart "Add to cart"
FW_WebButton "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Add to cart" @@ script infofile_;_ZIP::ssf24.xml_;_
FW_TransactionEnd "Add to cart"

wait 3
FW_TransactionStart "View cart"
FW_Link "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","View cart"
FW_TransactionEnd "View cart"
 @@ script infofile_;_ZIP::ssf25.xml_;_
FW_TransactionStart "Go to checkout"
FW_WebButton "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Go to checkout"
FW_TransactionEnd "Go to checkout"
 @@ script infofile_;_ZIP::ssf26.xml_;_
FW_TransactionStart "Free Shipping on order over $50"
FW_WebRadioGroup "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","methodId","5"
FW_TransactionEnd "Free Shipping on order over $50" @@ script infofile_;_ZIP::ssf27.xml_;_

wait 3
FW_TransactionStart "Input OrderNote"
FW_WebEdit "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","notes",OrderNote
FW_TransactionEnd "Input OrderNote"
 
wait 3
FW_TransactionStart "Click Proceed to payment" @@ script infofile_;_ZIP::ssf28.xml_;_
FW_WebButton "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Proceed to payment" @@ script infofile_;_ZIP::ssf29.xml_;_
FW_TransactionEnd "Click Proceed to payment"

FW_TransactionStart "COD Cash On Delivery"
FW_WebRadioGroup "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","methodId","5"
FW_TransactionEnd "COD Cash On Delivery"

FW_TransactionStart "Place order"
FW_WebButton "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Place order" @@ script infofile_;_ZIP::ssf30.xml_;_
FW_TransactionEnd "Place order" @@ script infofile_;_ZIP::ssf31.xml_;_

FW_TransactionStart "Chack Message Thank you for your order"
FW_CheckWebElement "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Thank you for your order"
FW_TransactionEnd "Chack Message Thank you for your order"
 @@ script infofile_;_ZIP::ssf32.xml_;_
FW_TransactionStart "My account"
FW_WebElement "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","My account" @@ script infofile_;_ZIP::ssf33.xml_;_
FW_TransactionEnd "My account"

FW_TransactionStart "Log out"
FW_Link "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Log out" @@ script infofile_;_ZIP::ssf34.xml_;_
FW_TransactionEnd "Log out"
 @@ script infofile_;_ZIP::ssf35.xml_;_
